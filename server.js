const fs = require('fs')
const https = require('https')
const bodyParser = require('body-parser')
const express = require('express')
const app = express()
const cors = require('cors')

const config = require('config')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors())

let ConfigHttps = {
  ssl: {
    key: fs.readFileSync('./cert/localhost.key'),
    cert: fs.readFileSync('./cert/localhost.crt')
  }
}

app.listen(config.http.port, () =>
  console.log(`HTTP  server OK: http://${config.domain}:${config.http.port}`))

https.createServer(ConfigHttps.ssl, app).listen(443)
console.log(`HTTP  server OK: https://${config.domain}`)
app.all('*', require('./routes/index'))

module.exports = app
