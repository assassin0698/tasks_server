const express = require('express')
const router = express.Router()
const uuidv4 = require('uuid/v4')

const html = './html/index.html'
router.get('/', function (req, res) {
  res.sendfile(html)
})

// component req
const { AuthLogic } = require('./component/AuthLogic')
const {Task} = require('./component/tasks')
const logic = require('./component/Logic')

router.post('/', function (req, res) {
  res.send('post')
  console.log(req.body)
})

router.post('/authentication', function (req, res) {
  console.dir(req.body)
  let reqString = {
    login: req.body.login,
    password: req.body.password
  }
  const authLogic = new AuthLogic(reqString)
  authLogic.authentication(function (answer) {
    res.send(answer)

    console.log('index : ' + answer)
  }
  )
})

router.post('/registration', function (req, res) {
  let reqString = {
    login: req.body.login,
    password: req.body.password,
    name: req.body.name,
    subordinates: req.body.subordinates,
    salt: null,
    hashPassword: null
  }
  const logic = new AuthLogic(reqString)
  logic.registration(function (answer) {
    console.log('index22 : ' + answer)
    res.sendStatus(answer)
  })
})

router.post('/users', function (req, res) {
  let reqStr = {UUID: req.body}
  console.dir(reqStr)
  logic.FindUser(req.body.uuid, function (data) {
    res.end(JSON.stringify(data))
  })
})

router.post('/tasks', function (req, res) {
  switch (req.body.type) {
    case 'add':
      let setAdd = new Task(req.body)
      setAdd.addTask(function (code) {
        res.sendStatus(code)
      })
      break
    case 'get':
      switch (req.body.status) {
        case 'author':
          let get = new Task(req.body)
          get.getTaskAuthor(function (tasks) {
            if (tasks !== false) {
              res.end(tasks)
            } else { res.end(false) }
          })
          break
        case 'recipient':
          let tasks = new Task(req.body)
          tasks.getTaskRecipient(function (tasks) {
            if (tasks !== false) {
              res.end(tasks)
            } else { res.end(false) }
          })
      }
      break
    case 'update':
      let updateTask = new Task(req.body)
      updateTask.UpdateTask(function (code) {
        res.end(code)
      })
      break
  }
})

module.exports = router
