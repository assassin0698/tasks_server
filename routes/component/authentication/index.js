// dependency declaration
const crypto = require('crypto')
const { MongoCon } = require('./../mongo_con')

// class Authentication
class Authentication {
  constructor (reqString) {
    this.reqString = reqString
  }

  async GeneratingHashForVerification (callback) {
    let newhash = null
    let mongocon = new MongoCon(this.reqString)
    let password = this.reqString.password
    mongocon.findUser(function (answer) {
      if (answer !== null) {
        newhash = crypto.createHash('sha512')
          .update(answer.salt + password, 'utf8')
          .digest('hex')
      } else {
        newhash = 'error'
      }
      // generated hash protect
      return callback(newhash)
    })
  }

  GeneratingHash (callback) {
    let salt = Math.round(Date.now() * Math.random())
    let hashpassword = crypto.createHash('sha512')
      .update(salt + this.reqString.password, 'utf8')
      .digest('hex')
    callback(hashpassword, salt)
  }
}

// exports class
exports.Authentication = Authentication
