var crypto = require('crypto')

const { MongoCon } = require('./mongo_con')
const {Authentication} = require('./authentication')

class AuthLogic {
  constructor (reqString) {
    this.reqString = reqString
  }

  registration (callback) {
    let hash = null
    let salt = null
    let authentical = new Authentication(this.reqString)
    authentical.GeneratingHash(function (genPassword, genSalt) {
      hash = genPassword
      salt = genSalt
    })
    this.reqString.salt = salt
    this.reqString.hashPassword = hash
    console.log(this.reqString)
    var reg = new MongoCon(this.reqString)
    reg.addUser(function (code) {
      if (code === 201) {
        reg.addUUID()
      }
    })
  }

  authentication (callback) {
    let req = this.reqString
    let status = null
    let doc = null
    let UserData = new MongoCon(req)
    UserData.findUser(function (docs) {
      if (docs === false) {
        return callback(false)
      } else {
        console.log('authlogic ' + docs)

        doc = docs
        let authentication = new Authentication(req)
        authentication.GeneratingHashForVerification(function (hash) {
          if (hash === doc.hashPassword) {
            UserData.UpdateUuid(function (status) {
              if (status) {
                UserData.findUser(function (data) {
                  return callback({login: data.login, uuid: data.UUID})
                })
              }
            })
          } else {
            status = false
            return callback(status)
          }
        })
      }
    })
  }

  recovery (callback) {
    let status = ''
    let salt = Math.round(Date.now() * Math.random())
    let hashpassword = crypto.createHash('sha512')
      .update(salt + pswd, 'utf8')
      .digest('hex')
    let queri = 'UPDATE auth SET password = \'' + hashpassword + '\', salt = \'' + salt + '\' WHERE login = \'' + login + '\''

    mysql.MysqlConnection(queri, function (err, next) {
      if (!err) {
        status = 'password recovery'
      } else {
        status = 'error'
      }
    })
    return callback(status)
  }
}
// export function
exports.AuthLogic = AuthLogic
