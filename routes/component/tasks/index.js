const { MongoCon } = require('./../mongo_con')

class Task {
  constructor (reqString) {
    this.reqString = reqString
  }

  addTask (callback) {
    console.dir(this.reqString)
    let mongocon = new MongoCon(this.reqString)
    mongocon.addTask(function (code) {
      return callback(code)
    })
  }

  getTaskAuthor (callback) {
    console.log(this.reqString)
    let mongocon = new MongoCon(this.reqString)
    mongocon.findUserUuid(function (data) {
      if (data !== false) {
        let mongocon1 = new MongoCon(data)
        mongocon1.getTaskAuthor(function (tasks) {
          return callback(tasks)
        })
      }
    })
  }

  getTaskRecipient (callback) {
    console.log(this.reqString)
    let mongocon = new MongoCon(this.reqString)
    mongocon.findUserUuid(function (data) {
      if (data !== false) {
        let mongocon1 = new MongoCon(data)
        mongocon1.getTaskRecipient(function (tasks) {
          return callback(tasks)
        })
      }
    })
  }

  UpdateTask (callback) {
    let doc = this.reqString
    console.log(this.reqString)
    let mongocon = new MongoCon(doc)
    mongocon.findUserUuid(function (data) {
      if (data !== false) {
        let mongocon1 = new MongoCon(doc)
        mongocon1.UpdateTask(function (tasks) {
          return callback(tasks)
        })
      }
    })
  }
}

exports.Task = Task
