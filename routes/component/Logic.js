const { MongoCon } = require('./mongo_con')

exports.FindUser = function (uuid, callback) {
  // console.log({UUID: uuid})
  let database = new MongoCon({UUID: uuid})
  database.findUserUuid1(function (data) {
    if (data) {
      return callback(data)
    } else {
      return callback(false)
    }
  })
}
