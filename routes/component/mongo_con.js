const MongoDB = require('mongoose')
const uuidv4 = require('uuid/v4')
// const config = require('config')

const Schema = MongoDB.Schema
MongoDB.Promise = global.Promise

const UserScheme = new Schema({
  _id: String,
  name: String,
  login: String,
  hashPassword: String,
  salt: String,
  subordinates: Array,
  UUID: String
})

const GroupsScheme = new Schema({
  name: String,
  subscribe: String
})

const TaskScheme = new Schema({
  title: String,
  text: String,
  answer: String,
  author: String,
  recipient: String,
  redirected: String
})

class MongoCon {
  constructor (reqString) {
    this.JsonString = reqString
  }

  addUser (callback) {
    MongoDB.connect('mongodb://192.168.1.125:27017/tasks')
    let User = MongoDB.model('User', UserScheme)
    let user = new User({
      name: this.JsonString.name,
      login: this.JsonString.login,
      hashPassword: this.JsonString.hashPassword,
      salt: this.JsonString.salt,
      subordinates: this.JsonString.subordinates
    })
    user.save(function (err) {
      MongoDB.disconnect()
      if (!err) {
        console.log('successful create user!!')
        return callback(201)
      } else {
        console.log('error create user!!')
        return callback(500)
      }
    })
  }

  addTask (callback) {
    MongoDB.connect('mongodb://192.168.1.125:27017/tasks')
    let Task = MongoDB.model('Task', TaskScheme)
    let task = new Task({
      title: this.JsonString.title,
      text: this.JsonString.text,
      answer: this.JsonString.answer,
      author: this.JsonString.author,
      recipient: this.JsonString.recipient,
      redirected: this.JsonString.redirected,
      UUID: null
    })
    task.save(function (err) {
      MongoDB.disconnect()
      if (!err) {
        console.log('successful create task!!')
        return callback(200)
      } else {
        console.log('error create task!!')
        return callback(500)
      }
    })
  }

  findUser (callback) {
    MongoDB.connect('mongodb://192.168.1.125:27017/tasks', function (err) {
      if (err) console.log(err)
    })
    let User = MongoDB.model('User', UserScheme)
    User.findOne({login: this.JsonString.login}, function (err, docs) {
      MongoDB.disconnect()
      if (err) return console.log('fund user: ' + err)
      if (docs === null) {
        return callback(false)
      } else {
        return callback(docs)
      }
    })
  }

  findUserUuid (callback) {
    MongoDB.connect('mongodb://192.168.1.125:27017/tasks', function (err) {
      if (err) console.log(err)
    })
    let User = MongoDB.model('User', UserScheme)
    User.findOne({UUID: this.JsonString.UUID}, function (err, docs) {
      MongoDB.disconnect()
      if (err) return console.log('find user error: ' + err)
      if (docs === null) {
        return false
      } else {
        return callback(docs)
      }
    })
  }

  findUserUuid1 (callback) {
    MongoDB.connect('mongodb://192.168.1.125:27017/tasks', function (err) {
      if (err) console.log(err)
    })
    let User = MongoDB.model('User', UserScheme)
    User.findOne({UUID: this.JsonString.UUID}, function (err, docs) {
      MongoDB.disconnect()
      if (err) return console.log('find user error: ' + err)
      if (docs === null) {
        return false
      } else {
        return callback(docs)
      }
    })
  }

  UpdateUuid (callback) {
    let login = this.JsonString.login
    let strAnswer = null
    let uuid = uuidv4()
    MongoDB.connect('mongodb://192.168.1.125:27017/tasks', function (err) {
      if (err) console.log(err)
      let User = MongoDB.model('User', UserScheme)
      User.updateOne(
        {login: login},
        { $set: {UUID: uuid}},
        function (err) {
          strAnswer = !err
          MongoDB.disconnect()
          return callback(strAnswer)
        }
      )
    })
  }

  getTaskAuthor (callback) {
    let data = this.JsonString
    MongoDB.Promise = global.Promise
    MongoDB.connect('mongodb://192.168.1.125:27017/tasks')
    console.log(data)
    let Task = MongoDB.model('Task', TaskScheme)
    Task.find({author: data.login}, function (err, docs) {
      MongoDB.disconnect()
      if (err) {
        console.log(err)
        return callback(false)
      } else {
        let tasks = JSON.stringify(docs)
        return callback(tasks)
      }
    })
  }

  getTaskRecipient (callback) {
    let data = this.JsonString
    MongoDB.Promise = global.Promise
    MongoDB.connect('mongodb://192.168.1.125:27017/tasks')
    console.log(data)
    let Task = MongoDB.model('Task', TaskScheme)
    Task.find({recipient: data.login}, function (err, docs) {
      MongoDB.disconnect()
      if (err) {
        console.log(err)
        return callback(false)
      } else {
        let tasks = JSON.stringify(docs)
        return callback(tasks)
      }
    })
  }

  UpdateTask (callback) {
    let id = this.JsonString.id
    let text = this.JsonString.text
    console.log('id: ' + id + ' text: ' + text)
    MongoDB.connect('mongodb://192.168.1.125:27017/tasks', function (err) {
      if (err) console.log(err)
      let task = MongoDB.model('Task', TaskScheme)
      task.updateOne(
        { _id: id },
        { $set: { answer: text}},
        function (err) {
          MongoDB.disconnect()
          if (!err) {
            return callback(JSON.stringify(true))
          } else {
            return callback(JSON.stringify(false))
          }
        }
      )
    })
  }
}

// export function
exports.MongoCon = MongoCon
